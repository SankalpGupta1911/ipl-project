const csv = require('csvtojson');
const path = require('path');
const fs = require('fs');

const deliveries = path.join(__dirname, './src/data/deliveries.csv');
const matches = path.join(__dirname, './src/data/matches.csv');

//Questions to solve:

// Question1: Number of matches played per year for all the years in IPL.
csv()
.fromFile(matches)
.then((jsonObj)=>{
    const matchesPerYear = require(path.join(__dirname, './src/server/matchesPerYear.cjs'));

    const result = matchesPerYear(jsonObj);

    fs.writeFileSync(path.join(__dirname,'./src/public/output/matchesPerYear.json'), JSON.stringify(result), "utf-8");
})

// Question2: Number of matches won per team per year in IPL.
csv()
.fromFile(matches)
.then((jsonObj)=>{

    let newJsonObj = jsonObj.filter((match)=> match.winner !== '');
    const matchesWonPerTeamPerYear = require(path.join(__dirname,'./src/server/matchesWonPerTeamPerYear.cjs'));

    const result = matchesWonPerTeamPerYear(newJsonObj);

    fs.writeFileSync(path.join(__dirname,'./src/public/output/matchesWonPerTeamPerYear.json') , JSON.stringify(result) , "utf-8")

})

//Question 3: Extra runs conceded per team in the year 2016

csv()
.fromFile(matches)
.then((matches)=>{

    csv()
    .fromFile(deliveries)
    .then((deliveries)=>{
        
        const extraRunsConcededPerTeam = require(path.join(__dirname, './src/server/extraRunsConcededPerTeam.cjs'));
    
        const result = extraRunsConcededPerTeam(matches,deliveries);
    
        fs.writeFileSync(path.join(__dirname,'./src/public/output/extraRunsConcededPerTeam.json'), JSON.stringify(result), "utf-8")

    })
})


// Question 4: Top 10 economical bowlers in the year 2015

csv()
.fromFile(deliveries)
.then((deliveries)=>{
    csv()
    .fromFile(matches)
    .then((matches)=>{

        const topEconomicalBowlers = require(path.join(__dirname, './src/server/topEconomicalBowlers.cjs'));

        const result = topEconomicalBowlers(matches, deliveries);
        
        fs.writeFileSync(path.join('./src/public/output/topEconomicalBowlers.json'), JSON.stringify(result), "utf-8");

    })

})

// Question 5: Find the number of times each team won the toss and also won the match

csv()
.fromFile(matches)
.then((matches)=>{

    filteredMatches = matches.filter( match => match.winner !=='' && match.toss_winner !=='');
    const wonTossAndWonMatch = require(path.join(__dirname, './src/server/wonTossAndWonMatch.cjs'));

    const result = wonTossAndWonMatch(matches);

    fs.writeFileSync(path.join(__dirname, './src/public/output/wonTossAndWonMatch.json'), JSON.stringify(result), "utf-8");

});

// Question 6: Find a player who has won the highest number of Player of the Match awards for each season

csv()
.fromFile(matches)
.then((matches)=> {

    const manOfTheMatchPerSeason = require(path.join(__dirname, './src/server/manOfTheMatchPerSeason.cjs'));

    const result = manOfTheMatchPerSeason(matches);

    fs.writeFileSync(path.join(__dirname, './src/public/output/manOfTheMatchPerSeason.json'), JSON.stringify(result), 'utf-8');
    
})


// Question 7: Find the strike rate of a batsman for each season

csv()
.fromFile(deliveries)
.then((deliveries)=> {

    csv()
    .fromFile(matches)
    .then((matches)=>{

    const batsmanStrikeRatePerSeason = require(path.join(__dirname, './src/server/batsmanStrikeRatePerSeason.cjs'));

    const result = batsmanStrikeRatePerSeason(matches, deliveries);

    fs.writeFileSync(path.join(__dirname, './src/public/output/batsmanStrikeRatePerSeason.json'), JSON.stringify(result), "utf-8")

    })

})

// Question 8: Find the highest number of times one player has been dismissed by another player

csv()
.fromFile(deliveries)
.then((deliveries)=>{

    const playerDismissedByABowlerMax = require(path.join(__dirname, './src/server/playerDismissedByABowlerMax.cjs'));

    const result = playerDismissedByABowlerMax(deliveries);

    fs.writeFileSync(path.join(__dirname, './src/public/output/playerDismissedByABowlerMax.json'), JSON.stringify(result), "utf-8");

})

// Find the bowler with the best economy in super overs

csv()
.fromFile(deliveries)
.then((deliveries)=>{

    const bestBowlerForSuperOver = require(path.join(__dirname, './src/server/bestBowlerForSuperOver.cjs'));

    const result = bestBowlerForSuperOver(deliveries);

    fs.writeFileSync(path.join(__dirname, './src/public/output/bestBowlerForSuperOver.json'), JSON.stringify(result), "utf-8");

})