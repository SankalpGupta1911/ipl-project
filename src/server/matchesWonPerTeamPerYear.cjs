function matchesWonPerTeamPerYear (matches){

    let matchesWonPerTeam = matches.reduce((acc, iter)=>{
        acc[iter.season] = acc[iter.season] || {};

        acc[iter.season][iter.winner] = acc[iter.season][iter.winner] || 0;
        acc[iter.season][iter.winner]++;
        
        return acc;
    },{})

    return matchesWonPerTeam;

}

module.exports = matchesWonPerTeamPerYear;