function matchesPerYear (matches) {

    return matches.reduce((acc, iter)=>{
        acc[iter['season']] = acc[iter['season']] || 0;
        acc[iter['season']] +=1;

        return acc;
    },{});
}

module.exports = matchesPerYear;