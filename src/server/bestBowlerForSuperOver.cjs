module.exports = function bestBowlerForSuperOver(deliveries){
    filteredDeliveries = deliveries.filter( delivery => delivery.is_super_over === '1');

    let bowlerEfficiency = {}

    filteredDeliveries.reduce((acc, iter)=> {

        acc[iter.bowler] = acc[iter.bowler] || [0,0];

        acc[iter.bowler][0]+=1;
        acc[iter.bowler][1]+=parseInt(iter.total_runs)

        let efficiency = (acc[iter.bowler][1] / (acc[iter.bowler][0] / 6));

        bowlerEfficiency[iter.bowler] = efficiency;

        return acc;

    },{})

    let array = Object.entries(bowlerEfficiency);
    array.sort((a,b)=> a[1] - b[1])
    let result = Object.fromEntries(array.slice(0,1))

    return result;
}