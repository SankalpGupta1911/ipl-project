module.exports = function playerDismissedByABowlerMax(deliveries) {
  filteredDeliveries = deliveries.filter(
    (delivery) => delivery.player_dismissed
  );

  let maxDismissalByPlayer = ["", "", 0];

  let allStats = filteredDeliveries.reduce((acc, iter) => {
    acc[iter.player_dismissed] = acc[iter.player_dismissed] || {};

    acc[iter.player_dismissed][iter.bowler] =
      acc[iter.player_dismissed][iter.bowler] || 0;
    acc[iter.player_dismissed][iter.bowler] += 1;

    if (acc[iter.player_dismissed][iter.bowler] >= maxDismissalByPlayer[2]) {
      maxDismissalByPlayer[0] = iter.player_dismissed;
      maxDismissalByPlayer[1] = iter.bowler;
      maxDismissalByPlayer[2] = acc[iter.player_dismissed][iter.bowler];
    }

    return acc;
  }, {});

  return allStats;
}