function wonTossAndWonMatch(matches){
    
    let result = matches.filter(match => match.winner === match.toss_winner).reduce((acc, iter)=>{
        acc[iter.winner] = acc[iter.winner] || 0;
        acc[iter.winner] += 1;

        return acc;
    },{})
    
    return result;
};

module.exports = wonTossAndWonMatch;