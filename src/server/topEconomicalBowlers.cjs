
function topEconomicalBowlers(matches, deliveries, top=10, year='2015'){

    let newMatches = matches.reduce((acc, iter)=> {
        if(iter.season === year){
            acc[iter.id] = iter.season;
        }
        return acc;
    },{}) 
    
    let bowlerAllStats = {}

    newDeliveries = deliveries.filter((delivery)=> newMatches[delivery.match_id])
    let bowlerEconomy = newDeliveries.reduce((acc,curr)=>{
    
        bowlerAllStats[curr.bowler] = bowlerAllStats[curr.bowler] || [0, 0, 0];
        
        bowlerAllStats[curr.bowler][0] += 1;
        
        bowlerAllStats[curr.bowler][1] += parseInt(curr.total_runs)
        
        bowlerAllStats[curr.bowler][2] = bowlerAllStats[curr.bowler][1]/(bowlerAllStats[curr.bowler][0]/6);
        
        acc[curr.bowler] = bowlerAllStats[curr.bowler][2];
        return acc;

    },{})

    const arrayBowlerEconomy = Object.entries(bowlerEconomy);
    arrayBowlerEconomy.sort((a,b)=> a[1]-b[1])

    return Object.fromEntries(arrayBowlerEconomy.slice(0,top));

}

module.exports = topEconomicalBowlers;