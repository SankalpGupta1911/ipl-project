function manOfTheMatchPerSeason(matches){

    let playerOfYear={};

    matches.filter(match=> match.player_of_match !=='').reduce((acc, iter)=>{

        acc[iter.season] = acc[iter.season] || {};

        acc[iter.season][iter.player_of_match] = acc[iter.season][iter.player_of_match] || 0;
        acc[iter.season][iter.player_of_match]+=1;

        playerOfYear[iter.season] = playerOfYear[iter.season] || iter.player_of_match;

        if(acc[iter.season][iter.player_of_match] > acc[iter.season][playerOfYear[iter.season]] ){
            playerOfYear[iter.season] = iter.player_of_match
        }

        return acc;

    },{})

    return playerOfYear;

}

module.exports = manOfTheMatchPerSeason;