function extraRunsConcededPerTeam (matches, deliveries,reqYear = '2016'){

    let matches2016 = matches.filter( match => match.season === reqYear).reduce((acc,iter)=>{
        acc[iter.id] = iter.season;
        return acc;
    },{})
    
    let result = deliveries.reduce((acc,iter) => {
        if(matches2016[iter.match_id]){
            acc[iter.bowling_team] = acc[iter.bowling_team] || 0;
            acc[iter.bowling_team] += parseInt(iter.extra_runs);
        }
        return acc;
    },{})

    return result;
}

module.exports = extraRunsConcededPerTeam;