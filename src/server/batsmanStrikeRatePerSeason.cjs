function batsmanStrikeRatePerSeason(matches,deliveries){

    matchIdSeason = matches.reduce((acc, iter)=>{
        acc[iter.id] = iter.season;
        return acc;
    },{})

    batsmanStats={}

    let perYearBatsmanStrikeRate = deliveries.reduce((acc, delivery) => {

        batsmanStats[matchIdSeason[delivery.match_id]] = batsmanStats[matchIdSeason[delivery.match_id]] || {}

        batsmanStats[matchIdSeason[delivery.match_id]][delivery.batsman] = batsmanStats[matchIdSeason[delivery.match_id]][delivery.batsman] || [0, 0]

        batsmanStats[matchIdSeason[delivery.match_id]][delivery.batsman][0] += 1;
        
        batsmanStats[matchIdSeason[delivery.match_id]][delivery.batsman][1] += parseInt(delivery.batsman_runs)

        acc[matchIdSeason[delivery.match_id]] = acc[matchIdSeason[delivery.match_id]] || {};

        acc[matchIdSeason[delivery.match_id]][delivery.batsman] = (batsmanStats[matchIdSeason[delivery.match_id]][delivery.batsman][1] / batsmanStats[matchIdSeason[delivery.match_id]][delivery.batsman][0])*100 ; 

        return acc;
    },{})
    return perYearBatsmanStrikeRate;
};
module.exports = batsmanStrikeRatePerSeason;